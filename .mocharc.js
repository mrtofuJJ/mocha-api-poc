'use strict';

// This is a JavaScript-based config file containing every Mocha option plus others.
// If you need conditional logic, you might want to use this type of config.
// Otherwise, JSON or YAML is recommended.

module.exports = {
    color: true,
    extension: ['js'],
    growl: false,
    package: './package.json',
    parallel: false,
    slow: '75',
    sort: false,
    spec: ['test/**/*.js'], // the positional arguments!
    timeout: '10000', // same as "timeout: '2s'" timeout: false, // same as "'no-timeout': true" or "timeout: 0"
    'trace-warnings': true, // node flags ok
    ui: 'bdd',
    'v8-stack-trace-limit': 100, // V8 flags are prepended with "v8-"
    watch: false,
    'watch-files': ['lib/**/*.js', 'test/**/*.js'],
    'watch-ignore': ['lib/vendor'],
    reporter: 'mochawesome',
    retries: 1,
    //'reporter-option': ['foo=bar', 'baz=quux'],
    require: ['lib/function/fixtures.js'],
    //'allow-uncaught': false,
    //'async-only': false,
    //bail: false,
    //'check-leaks': false,
    //delay: false,
    //diff: true,
    //'forbid-only': false,
    //'forbid-pending': false,
    //'full-trace': false,
    //exit: false, // could be expressed as "'no-exit': true"
    //fgrep: something, // fgrep and grep are mutually exclusive
    //grep: /People Identity/,
    //file: ['/path/to/some/file', '/path/to/some/other/file'],
    //global: ['jQuery', '$'],
    //ignore: ['test/people/*'],
    //'inline-diffs': false,
    // invert: false, // needs to be used with grep or fgrep
    //jobs: 1,
};