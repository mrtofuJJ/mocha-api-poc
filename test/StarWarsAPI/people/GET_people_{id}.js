const { starWarsAPIConfig:config, parallel, supertest, expect, addContext } = require('../../../lib/function/requires.js')

const request = supertest(config.baseUrl)
const identities = [
    {id: "1",obj: {name: "Luke Skywalker",height: "172",mass: "77"}},
    {id: "2",obj: {name: "C-3PO",height: "167",mass: "75"}},
    {id: "3",obj: {name: "R2-D2",height: "96",mass: "32"}},
    {id: "4",obj: {name: "Darth Vader",height: "202",mass: "136"}},
]

describe.only('People Identity', function() {
    for (const {id,obj} of identities) {
        it(`GET /people/${id} for ${obj.name}`, async function() {
            const res =await request
                .get(`/people/${id}`)
                .set({Accept: config.header.accept, "Content-Type": config.header.contentType})
                .expect(200)
            addContext(this, {title: 'People Identity assertion', value:{obj}})
            expect(res.body).to.deep.include(obj)
        });
    }
});