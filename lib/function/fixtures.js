'use strict'
const { argv } = require('./argvs.js')

exports.mochaGlobalSetup = function () {
    console.log(`=====================================`)
    console.log(`      Current execution arguments    `)
    for (const arg in argv) {    
        console.log(`${arg}: ${argv[arg]}`)
    }
    console.log(`=====================================`)
}