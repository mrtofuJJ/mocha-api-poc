const { argv } = require('./argvs.js')
const { config:goRestAPIConfig } = require(`../config/GoRestAPI/${argv.env}.js`)
const { config:starWarsAPIConfig } = require(`../config/StarWarsAPI/${argv.env}.js`)
const parallel = require('mocha.parallel')
const supertest = require('supertest');
const { expect, should, assert } = require('chai');
const addContext = require('mochawesome/addContext');

module.exports = {
    argv,
    goRestAPIConfig,
    starWarsAPIConfig,
    parallel,
    supertest,
    expect,
    should,
    assert,
    addContext
}