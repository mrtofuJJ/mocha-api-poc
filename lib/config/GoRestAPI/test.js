const apiAccessToken=`4bf3f1a11425ae5b12aa4c3708de32cc9ab599dd3334372487843ffc717f12f7`
exports.config = {
    baseUrl: "https://gorest.co.in/public/v1/users",
    apiAccessToken: apiAccessToken,
    header: {
        accept: "application/json",
        contentType: "application/json",
        authorization: apiAccessToken
    },
}